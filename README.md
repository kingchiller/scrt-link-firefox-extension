<img src="icon-128.png" width="64"/>

# Firefox Addon for scrt.link

- [Firefox Addon](https://addons.mozilla.org/en-US/developers/addon/scrt-link)
- Microsoft Edge Extension (Same package - Link coming soon)

Basically just wraps https://scrt.link/widget in an iframe.

## Install

Install dependency: [web-ext](https://github.com/mozilla/web-ext)

```sh
# Run locally
web-ext run

# Build for Firefox Addons store
web-ext build

```

### Licence

MIT
